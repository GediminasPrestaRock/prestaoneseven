{shop_url}

Sveiki, {firstname} {lastname},

Užsakymo ID {order_name} - Grąžinimo aktas apdorotas

Mes atlikome grąžinimą už Jūsų užsakymą {order_name} iš {shop_name}.

Follow your order and download your invoice on our shop, go to the [Užsakymų istorija ir informacija]({history_url}) section of your customer account.

If you have a guest account, you can follow your order via the [Siuntinio sekimas]({guest_tracking_url}) section on our shop.

[{shop_name}]({shop_url})

Powered by [PrestaShop](https://www.prestashop.com/?utm_source=marchandprestashop&utm_medium=e-mail&utm_campaign=footer_1-7)
